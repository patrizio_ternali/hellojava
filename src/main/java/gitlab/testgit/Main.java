package gitlab.testgit;

public class Main {

	public static void main(String[] args) {
		
		Parallelogramma p = new Parallelogramma(3,6,5);

		System.out.println(p.perimetro(p.getLatoA(), p.getLatoB()));
		System.out.println(p.area(p.getLatoB(), p.getAltezza()));
		System.out.println(p.latoObliquo(p.getPerimetro(), p.getLatoB()));
		
	}
}
