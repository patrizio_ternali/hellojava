package gitlab.testgit;

public class Parallelogramma {

	private int latoA;
	private int latoB;
	private int altezza;
	
	private int perimetro;
	private int area;
	private int latoObliquo;
	
	public Parallelogramma(int latoA,int latoB, int altezza) {
		this.latoA = latoA;
		this.latoB = latoB;
		this.altezza = altezza;
	}
	
	public int perimetro(int latoA, int latoB) {
		perimetro = (latoA*2) + (latoB*2);
		return perimetro;
	}
	
	public int area(int latoB, int altezza) {
		area = (latoB*altezza);
		return area;
	}
	
	public int latoObliquo(int perimetro, int latoB) {
		latoObliquo = (perimetro*2 - latoB*2) / 2;
		return latoObliquo;
	}

	public int getLatoA() {
		return latoA;
	}

	public int getLatoB() {
		return latoB;
	}

	public int getAltezza() {
		return altezza;
	}

	public int getPerimetro() {
		return perimetro;
	}

	public int getArea() {
		return area;
	}
	
	
}
